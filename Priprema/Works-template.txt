- var lang = "Eng"
- var works = "whoredom"

doctype html
html(lang = "en")
  head
    case lang
      when "Eng"
        include includes/head
      default
        include ../includes/head
    title Whoredom
  body
    #wrapper
      header#header
        h1: a(href="index.html") Sonja Radaković
      nav#nav
        include includes/menu/menu
      .container
        case works
          when "whoredom"
            include includes/container/works/01-whoredom/whoredom
          when "installations"
            include includes/container/works/02-installations/installations
          when "infections"
            include includes/container/works/03-infections/infections
          when "paintings"
            include includes/container/works/04-paintings/paintings
      footer#footer
        p &copy; by Sonja Radaković
      #script
        case lang
          when "Eng"
            include includes/scripts
          default
            include ../includes/scripts
