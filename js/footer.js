var footerFixed = function(){
  var height = $(window).height(); //visina browsera
  var wrapper = $("#wrapper").height(); // visina HTML
  var $footer = $('#footer');
  if(height >= wrapper){
    $footer.addClass("navbar-fixed-bottom");
  }
  else if (wrapper>=height && $footer.hasClass("navbar-fixed-bottom")){
    $footer.removeClass("navbar-fixed-bottom");
}}
$(document).ready(footerFixed);
$(window).resize(footerFixed);
$(window).scroll(footerFixed);